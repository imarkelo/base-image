# Smoke tests - OSTree

Smoke tests for OSTree which RHIVOS image should be based on

## This Test Suite includes these tests

1. Test `ostree admin status` returns 0 meaning the image is OSTree based

2. Check if `composefs` is enabled and mounted by default in OSTree based images

3. Check `prepare-root.conf` file within initrd of the signed OSTree image

