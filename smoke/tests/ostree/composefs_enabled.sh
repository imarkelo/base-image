#!/bin/bash

set -uxo pipefail

# Check if the running image is an ostree-based image
if [ ! -e /run/ostree-booted ]; then
   echo "FAIL: Non-ostree images cannot be used for this test!"
   exit 1
fi

# Check if composefs is mounted on / and enabled
COMPOSEFS_MOUNT_POINT=$(mount | grep "^composefs on / type overlay")
if [[ $COMPOSEFS_MOUNT_POINT ]]; then
    if [[ $COMPOSEFS_MOUNT_POINT =~ ^.*verity=require.*$ ]]; then
        echo "PASS: Composefs is mounted on / and verity option is set to require"
        RC=0
    else
        echo "FAIL: Composefs is mounted on / but verity option is not set to require"
        RC=1
  fi
else
    echo "FAIL: Composefs is not mounted on /"
    RC=1
fi

exit $RC