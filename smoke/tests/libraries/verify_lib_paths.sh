#!/bin/bash

set -euo pipefail

# Checking ldconfig cache
# Define the list of libraries to check
LIBS=("libc.so.6" "libstdc++.so.6")

# Loop through each library to verify its path
for lib in "${LIBS[@]}"; do
    # Retrieve the library path from ldconfig
    lib_path=$(ldconfig -p | grep "$lib" | awk '{print $NF}')
    
    # The library is not found in ldconfig cache
    if [ -z "$lib_path" ]; then
        echo "FAIL: $lib not found in ldconfig cache!"
        exit 1
    fi

    # Check if the library path is a real file and not a symbolic link
    if [ -f "$lib_path" ] && [ ! -h "$lib_path" ]; then
        echo "PASS: $lib is correctly registered as a file: $lib_path"
    else
        if [ "$lib" == "libstdc++.so.6" ]; then
            # Special case: Print failure but do NOT exit for now
            echo "FAIL (Expected): $lib points to a symlink for now or does not exist: $lib_path"
        else
            echo "FAIL: $lib points to a symlink or does not exist: $lib_path"
            exit 1
        fi
    fi

# Ensure that the dynamic linker resolves the DT_NEEDED entries correctly.
# The dynamic linker must resolve required libraries (e.g., libc.so.6, libstdc++.so.6)
# to the exact same paths found through `ldconfig -p`.
# - libc.so.6 should resolve to $lib_path found via ldconfig.
# - libstdc++.so.6 must be tested using a C++ binary because it's only loaded when needed.

    # Verify dynamic linker resolution
    if [ "$lib" == "libstdc++.so.6" ]; then
        # Run rpm-ostree --help and check libstdc++ resolution
	resolved_lib=$(LD_TRACE_LOADED_OBJECTS=1 rpm-ostree --help | grep "$lib" | awk '{print $3}')
    else
        # Run bash dynamically linked to libc.so.6 and check its resolution
        resolved_lib=$(LD_TRACE_LOADED_OBJECTS=1 /bin/bash | grep "$lib" | awk '{print $3}')
    fi

    # Compare the resolved path with ldconfig path
    if [ "$resolved_lib" == "$lib_path" ]; then
        echo "PASS: $lib correctly resolves to: $resolved_lib"
    else
        echo "FAIL: Mismatch - ldconfig found $lib at $lib_path but dynamic linker resolved it to $resolved_lib"
        exit 1
    fi

done   
