# Smoke tests - Modules

Smoke test to verify disabled modules.

## This Test Suite includes these tests

1. Confirm that nfs related modules are disabled.
[nfs_modules_disabled.sh]

2. Confirm that vfat related modules are disabled.
[vfat_modules_disabled.sh]