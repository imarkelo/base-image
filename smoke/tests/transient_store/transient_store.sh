#!/bin/bash

# Insertion of storage.conf path as a string value in to declared storage_conf parameter.
storage_conf="/etc/containers/storage.conf"

# Check if /etc/containers/storage.conf doesn't exists.
if [ ! -f $storage_conf ]; then
    echo "INFO : ${storage_conf} does not exists!"
    echo "FAIL : $(basename "$0" .sh)"
    # Exit code as no storage.conf exists on a system.
    exit 1
fi

# Insertion of "transient_store = true" as a string value in to declared transient_store parameter.
transient_store="transient_store = true"

# Check if transient_store is set to "true".
if grep -Fxq "$transient_store" "$storage_conf"; then
    # Transient store is set to true.
    echo "INFO : Transient_store is set to true in configuration file $storage_conf."
    echo "PASS : $(basename "$0" .sh)"
    exit 0
else
    # Check to what value transient_store is set.
    transient_store_value="$(grep -F "transient_store" "$storage_conf")"

    # Report the Transient_store is disabled and its value and exit.
    echo "INFO : Transient_store is disabled and set to: ${transient_store_value}"
    echo "FAIL : $(basename "$0" .sh)"
    exit 1
fi

