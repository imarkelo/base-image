# Smoke tests - Transient_store 

Smoke test for enabled transient_store on provided running image.

## This Test Suite includes these tests:

1. Confirm that transient_store is enabled by default for RHIVOS images.
    
