#!/bin/bash

#Check if the system has an Ostree image or exit.
#Check image type of this OS and save value to image_type parameter.
# shellcheck source=/dev/null
source /etc/build-info

#Check if the system has an Ostree image or exit.
if [ ! -e /run/ostree-booted ]; then
	echo "FAIL: Non-ostree images cannot be used for this test!"
	exit 1
fi

#Checking public mirrors response.

#Declaration of arch parameter with string value "arch".
architec=$(arch)

#Iput expected URL to expected_url parameter, using "arch" as variable for genericly resolved arch variable.
#The test runs on both architectures (aarch64 and x86_64), so we need a generic $arch variable.
expected_url="https://mirror.stream.centos.org/SIGs/9-stream/automotive/${architec}/packages-main/repodata/repomd.xml"

#Declaration of check_url function, which checks if the URL is reachable.
check_url() {
  curl --output /dev/null --silent --head --fail "$1"
}

#Check if URL is reachable.
if ! check_url "$expected_url" ; then

        #Exit if URL is unreachable.
        echo "ERROR: The test failed because the mirror is not responding"
        exit 1
fi

#Check if there are local repos exists and if not, fetch from public repos.
#Insert path of local repositories on a system to declared repository_path parameter.
local_repo_path="/etc/yum.repos.d/"

#Check if /etc/yum.repos.d/ repository folder is empty.
if [ -z "$(ls -A $local_repo_path)" ]; then

        #Local repository folder /etc/yum.repos.d/ is empty.
        echo "Local repository folder $local_repo_path is empty."

        #Fetch public repository to declared rpm_ostree_repo parameter.
        # shellcheck disable=SC2016
        rpm_ostree_repo='[autosd]\nbaseurl=https://mirror.stream.centos.org/SIGs/$releasever-stream/automotive/$basearch/packages-main/\nenabled=1\ngpgcheck=0'

        #Insert local repo file in local repositories folder to declared repository_path parameter.
        local_repo_file="/etc/yum.repos.d/rpm-ostree-test.repo"

        #Create repository on host.
        echo -e "$rpm_ostree_repo" > "$local_repo_file"
else
        #Local repository folder /etc/yum.repos.d/ isn't empty.
        echo -e "Local repository folder $local_repo_path isn't empty.\nMoving all contents of /etc/yum.repos.d/ to /tmp/backuprepo."

	#Declare function backuprepo to move all the contents of the /etc/yum.repos.d/ to /tmp/backuprepo/.
        backuprepo () {
                mkdir /tmp/backuprepo && mv /etc/yum.repos.d/*.* /tmp/backuprepo/
        }

        #Call function backuprepo.
        backuprepo

        #Fetch public repository to declared rpm_ostree_repo parameter.
        # shellcheck disable=SC2016
        rpm_ostree_repo='[autosd]\nbaseurl=https://mirror.stream.centos.org/SIGs/$releasever-stream/automotive/$basearch/packages-main/\nenabled=1\ngpgcheck=0'

        #Insert local repo file in local repositories folder to declared repository_path parameter.
        local_repo_file="/etc/yum.repos.d/rpm-ostree-test.repo"

        #Create repository on host.
        echo -e "$rpm_ostree_repo" > "$local_repo_file"
fi

#Check if "rpm-ostree refresh-md" doesn't returns regression error or exit.
#Declare function rpm_ostree_cleanup to clean up the repository cash.
rpm_ostree_cleanup () {
        rpm-ostree cleanup -m
}

#Calls rpm-ostree_cleanup function to cleanup the repository cash.
rpm_ostree_cleanup

#Declare function rpm_ostree_refresh_md, which runs rpm-ostree refresh-md command.
rpm_ostree_refresh_md () {
        rpm-ostree refresh-md
}

#Check if rpm-ostree refresh-md command runs successfully.
if rpm_ostree_refresh_md; then
        echo "Command rpm-ostree refresh-md succeeded."
else
        #Declaration of check_error_404 function, which checks if the there was an error 404.
        check_error_404() {
                journalctl -b -l --since "1 minute ago" -r | grep "Status code: 404"
                }
        #Check if check_error_404 function found error 404.
        if check_error_404; then
                echo -e "\n\nError 404 found, pleashe check for regression bug.\n\n"
        else
                echo "No error 404 found."
        fi

        echo "Command rpm-ostree refresh-md failed."
        exit 1
fi

#The test was successful.

#Remove fetched repos from /etc/yum.repos.d/.
#Declare function cleanuprepo to remove fetched rpm-ostree-test.repo from /etc/yum.repos.d/.
cleanuprepo () {
	rm -rf /etc/yum.repos.d/rpm-ostree-test.repo
}

#Call function cleanuprepo.
cleanuprepo

#Move all contents of /tmp/backuprepo/ if exists, back to /etc/yum.repos.d/ and remove backuprepo folder from /tmp/.
#Declaration of backuprepo_dir parameter with "/tmp/backuprepo/" value.
backuprepo_dir="/tmp/backuprepo"

#Declare function restore_repo to restore all repositories from /tmp/backuprepo/ to /etc/yum.repos.d/.
restore_repo () {
	mv /tmp/backuprepo/*.* /etc/yum.repos.d/
}

#Declare function rm_backuprepo to remove backuprepo folder from /tmp/.
rm_backuprepo () {
	rm -rf /tmp/backuprepo
}

#Check if backuprepo directory exists and restore original repos.
if [ -d "$backuprepo_dir" ]; then
	
        #Call function restore_repo.
        restore_repo

        #Delete the /tmp/backuprepo folder.
        #Call function rm_backuprepo.
        rm_backuprepo

fi

#The cleanup was successful.
exit 0
