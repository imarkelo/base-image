#!/bin/bash

set -e

# Check podman is installed. The minimal image does NOT contain podman
if ! rpm -q podman > /dev/null; then
   echo "INFO : podman isn't installed."
   echo "PASS : $(basename "$0" .sh)"
   exit 0 
fi

# By default the podman service is NOT enabled. This is the expected behaviour.
if [ "$(systemctl is-enabled podman)" == "disabled" ]; then
   echo "INFO : Podman service is not enabled"
   echo "PASS : $(basename "$0" .sh)"
   exit 0
else
   echo "INFO : Podman service is enabled"
   echo "FAIL : $(basename "$0" .sh)"
   exit 1
fi
