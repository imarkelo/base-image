#!/bin/bash

if ! systemctl is-system-running --wait ; then
  echo "===== List of failed units ====="
  systemctl list-units --state=failed
  echo "======= Full system log ========"
  journalctl --boot
  exit 1
fi
