#!/bin/bash

set -e

# Check the oom_score_adj value of a regular process. Default: 0
CMD_OOM_SCORE_ADJ=$(cat "/proc/self/oom_score_adj")
if [ "$CMD_OOM_SCORE_ADJ" -eq 0 ]; then
   echo "PASS: regular process oom_score_adj value == 0"
else
   echo "FAIL: regular process oom_score_adj value != 0"
   exit 1
fi

# Check podman is installed. The minimal image does NOT contain podman
if ! rpm -q podman > /dev/null; then
	echo "INFO : podman isn't installed."
	echo "PASS : $(basename "$0" .sh)"
	exit 0
fi

# Check the oom_score_adj value for a podman container being executed. Default: 0
CONTAINER_OOM_SCORE_ADJ=$(podman run --rm --replace --name oom-test      \
                                 quay.io/centos/centos:stream9-minimal   \
                                 cat /proc/self/oom_score_adj 2> /dev/null)

if [ "$CONTAINER_OOM_SCORE_ADJ" -eq 0 ]; then
   echo "PASS: regular container oom_score_adj value == 0"
else
   echo "FAIL: regular container oom_score_adj value != 0"
   exit 1
fi
