#!/bin/bash

# Initialize avc_error_messages variable
avc_error_messages=""

# Function to check if dmesg process is running
check_dmesg_process() {
  if ! systemctl is-active systemd-journald &> /dev/null; then
    echo "FAIL : dmesg process (systemd-journald) is not running."
    exit 1
  fi
}

# Function to check for AVC errors in dmesg logs
check_avc_errors() {
  avc_error_messages=$(journalctl -k | grep "avc: ")
  if [ -n "$avc_error_messages" ]; then
    echo "FAIL : AVC errors detected in dmesg logs:"
    echo "$avc_error_messages"
    exit 1
  else
    echo "PASS : No AVC errors detected in dmesg logs."
  fi
}

# Call functions to perform checks
check_dmesg_process
check_avc_errors