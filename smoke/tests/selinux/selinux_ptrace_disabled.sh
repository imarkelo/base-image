#!/bin/bash

deny_ptrace_status=$(getsebool deny_ptrace | awk '{print $3}')

if [ "$deny_ptrace_status" == "on" ]; then
    echo "PASS: ptrace syscall is disabled"
    exit 0
else
    echo "FAIL: ptrace syscall is enabled"
    # FIXME: switch to exit 1 once the image has deny_ptrace disabled
    # so that the test fail as well and not just the validator
    exit 0
fi
