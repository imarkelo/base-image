# Smoke tests - general

Test cases about general of system.

## This Test Suite includes these tests

1. Test the system date is displayed correctly with no clock skew.
2. Test the system scheduler default configuration
3. Test the system kernel reboot on panic configuration
