# Base image smoke tests

Simple test suite of smoke tests for the base RHIVOS image

## To run the smoke tests locally
`tmt run plans -n /smoke/plans/core -vvv`

## To run the smoke tests on provision virtual
`tmt run -a -vvv plans -n /smoke/plans/core provision --how virtual --image https://cloud.centos.org/centos/9-stream/x86_64/images/CentOS-Stream-GenericCloud-9-20211119.0.x86_64.qcow2`

## To run the smoke tests on provision minute
`tmt run -a -vvv plans -n /smoke/plans/core provision --how minute --image 1MT-CentOS-Stream-9`

## To run specific plan for running tests for e.g. Testing Farm
`tmt run -a -vvv plans -n /smoke/plans/core provision --how connect -u root -p password -g 1.2.3.4`

## To run specific set of tests inside smoke directory
Just change the name filter for plans to you desired test plan, e.g.:
`tmt run -a -vvv tests -n /smoke/tests/ostree provision --how virtual --image https://cloud.centos.org/centos/9-stream/x86_64/images/CentOS-Stream-GenericCloud-9-20211119.0.x86_64.qcow2`

## This Test Suite includes multiple smoke tests

1. Boot tests - check that image is booted properly
2. Ostree tests - check if we are running Ostree image
3. Service tests - check default services are running and podman is disabled
4. General tests - contains smoke test not belonging to other categories, e.g. time skew
5. Selinux tests - check basic functionality and default state of SELinux on image

