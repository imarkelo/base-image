## Freedom From Interference (FFI) - Scheduler test

## Title: Scheduler Test Suite

### Description:
This test set should investigate the reduction in available CPU time for the host's application (sched_setscheduler/setpriority is called by the application) when CPU type load is applied to the system through a container.
Included test cases:

1. `scheduler_policies`
   1. Call sched_setscheduler(SCHED_FIFO) in the host application, finish some defined workload and measure the execution time
   2. Create lots of work for the CPU in a container
   3. Run the host application again, measure the execution time
   4. Measure the difference elapsed time for two runs
   5. The second run should not be slowed down more than 20%

2. `scheduler_priorities`
   1. Call setpriority(PRIO_PROCESS, getpid(), -20) in the host application, finish some defined workload and measure the execution time
   2. Create lots of work for the CPU in a container
   3. Run the host application again, measure the execution time
   4. Measure the difference elapsed time for two runs
   5. The second run should not be slowed down more than 20%

### Test input
Test image: quay.io/centos/centos:stream9
Testing tools: stress-ng

### Expected output
Additional Notes: The test result of `scheduler_policies` and `scheduler_priorities` depend on the test results of CpuTime, if the CpuTime interference test fails then the results from the tests here are also meaningless.

1. `scheduler_policies`
```
...
-- Starting test ...
Build the application...
Starting first run...
Running tst_scheduler_policies for the first time.
sched_prioriy is: xx
Scheduler policy of the current thread is SCHED_FIFO
Elapsed time for workload: xx.xxxxxx seconds
Number of primes up to 10000000: xxxxxx
Starting CPU-intensive tasks inside bad_container...
...
Installing stress-ng inside bad_container...
...
stress-ng command is: stress-ng --taskset x-x --cpu x --cpu-load 100 --timeout 120s
Running stress-ng in bad_container...
stress-ng has been successfully started.
Starting second run...
Running tst_scheduler_policies for the second time.
sched_prioriy is: xx
Scheduler policy of the current thread is SCHED_FIFO
Elapsed time for workload: xx.xxxxxx seconds
Number of primes up to 10000000: xxxxxx
Stopping stress-ng, killed processes:
xx xx xx...
First run took xx.xxxxxxs and the second run took xx.xxxxxxs
The time difference between two runs is x.xxxxxxs
The second run was x.xxxxx% slower than the first run, less than 20%.
Pass: No interference!
Starting cleanup...
Cleanup complete
bad_container
Nothing to clean up
...
```
1. `scheduler_priorities`
```
...
-- Starting test ...
Build the application...
Starting first run...
Running tst_scheduler_priorities for the first time.
setpriority(PRIO_PROCESS, getpid(), -20)
Elapsed time for workload: xx.xxxxxx seconds
Number of primes up to 10000000: xxxxxx
Starting CPU-intensive tasks inside bad_container...
...
Installing stress-ng inside bad_container...
...
stress-ng command is: stress-ng --taskset x-x --cpu x --cpu-load 100 --timeout 120s
Running stress-ng in bad_container...
stress-ng has been successfully started.
Starting second run...
Running tst_scheduler_priorities for the second time.
setpriority(PRIO_PROCESS, getpid(), -20)
Elapsed time for workload: xx.xxxxxx seconds
Number of primes up to 10000000: xxxxxx
Stopping stress-ng, killed processes:
xx xx xx...
First run took xx.xxxxxxs and the second run took xx.xxxxxxs
The time difference between two runs is x.xxxxxxs
The second run was x.xxxxxx% slower than the first run, less than 20%.
Pass: No interference!
Starting cleanup...
Cleanup complete
bad_container
Nothing to clean up
...
```
