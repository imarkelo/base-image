#!/bin/bash

# shellcheck source=SCRIPTDIR/../../common.sh
. "$(dirname "$(readlink --canonicalize "$0")")"/../../common.sh

# Build applications
build_application() {
    if ! test -f "$application_name"; then
        gcc -o "$application_name" "$application_source_file" prime.c -Wall -Werror
    fi
}

get_cpuset() {
    cpuset=$(cat /sys/devices/system/cpu/isolated)
    if [[ -z $cpuset ]]; then
        echo "/sys/devices/system/cpu/isolated is empty, using /sys/devices/system/cpu/online"
        cpuset=$(cat /sys/devices/system/cpu/online)
    fi

    # Convert CPU set to a sorted list
    for cpus in ${cpuset//,/ }; do
        if [[ $cpus =~ - ]]; then
            # shellcheck disable=SC2086
            cpus=$(seq ${cpus//-/ })
        fi
        cpus_list="$cpus_list $cpus"
    done
    cpus_list=$(echo "$cpus_list" | tr ' ' '\n' | grep -v ^$ | sort -nu | xargs echo)

    # Get the number of CPUs
    cpu_num=$(echo "$cpus_list" | awk '{print NF}')
}

create_tasks_in_container() {
    echo "Installing stress-ng inside $BAD_CONTAINER..."
    install_package_in_bad_container "stress-ng procps-ng util-linux"

    # Run stress-ng in container
    echo "Running stress-ng in $BAD_CONTAINER..."
    for cpu in ${cpus_list}; do
        echo "Starting stress-ng on CPU $cpu with $cpuload% load."
        podman exec -w /var "$BAD_CONTAINER" bash -c "taskset -c $cpu stress-ng --temp-path /tmp --cpu 1 --cpu-load $cpuload --timeout 600s" &
    done

    # Wait stress-ng to start
    wait_for_stress_ng_to_start "$cpuload" "$cpu_num"
}

compare_elapsed_time_for_two_runs() {
    # Get run time for the log file
    run_time1=$(grep "Elapsed time" "$log_of_run1" | awk -F: '{ print $NF }' | sed 's/\ //' | sed -e 's/ seconds//g')
    run_time2=$(grep "Elapsed time" "$log_of_run2" | awk -F: '{ print $NF }' | sed 's/\ //' | sed -e 's/ seconds//g')

    # Threshold is 20%
    max_rel_error=$(awk "BEGIN {print $run_time1/5}")
    run_time_difference=$(awk "BEGIN{print $run_time1 - $run_time2}")
    # abs()
    run_time_difference=${run_time_difference#-}
    second_run_slowdown=$(awk "BEGIN{print ($run_time_difference/$run_time1)*100}")

    echo "First run took ${run_time1}s and the second run took ${run_time2}s"
    echo "The time difference between two runs is ${run_time_difference}s"

    # The second run should not be slowed down more than 20%
    if [[ $(awk "BEGIN {print ($run_time_difference >= $max_rel_error)}") -gt 0 ]]; then
        echo "The second run was $second_run_slowdown% slower than the first run, over 20%."
        error "Fail: Interference is present!"
        exit 1
    else
        echo "The second run was $second_run_slowdown% slower than the first run, less than 20%."
        success "Pass: No interference!"
    fi
}


# arg1: name of test to run
if [[ -n "$1" ]]; then
    application_name="tst_$1"
    application_source_file="$1.c"
    log_of_run1="$1_run1.log"
    log_of_run2="$1_run2.log"
else
    echo "Please provide a test name to run, 'scheduler_priorities' or 'scheduler_policies'"
    exit 1
fi

# arg2: (optional) cpu load
# Default cpu load is 80%
cpuload=80
if [[ -n "$2" ]]; then
    cpuload="$2"
fi

# Build the application that runs on the host
echo "Build the application..."
build_application

# Execute the application for the first time on the host
echo "Starting first run..."
echo "Running $application_name for the first time."
./"$application_name" | tee -a "$log_of_run1"

# Start stress-ng inside container and let it last for 2 minutes
echo "Starting CPU-intensive tasks inside $BAD_CONTAINER..."
get_cpuset
create_tasks_in_container

# Execute the application for the second time on the host
echo "Starting second run..."
echo "Running $application_name for the second time."
./"$application_name" | tee -a "$log_of_run2"

# Stop stress-ng
stop_stress_ng

# Compare the time taken for the two runs and output the test results
compare_elapsed_time_for_two_runs

