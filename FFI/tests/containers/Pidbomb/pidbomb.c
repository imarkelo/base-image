// SPDX-License-Identifier: GPL-2.0-or-later
/*
 * Copyright Red Hat
 * Authors: Dennis Brendel <dbrendel@redhat.com>
 *          Barbora Dolezalova <bdolezal@redhat.com>
 */

#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sched.h>
#include <sys/mman.h>

#define STACK_SIZE (1024 * 1024)

/* The program attempts to create as many processes as possible using clone().
 * When it can no longer create processes, 
 * it prints the number of clones created.*/

static int idle_func (__attribute__((unused)) void *arg)
{
    sleep(60); // Simulate work in child process
    return 0;
}

static int do_test(void)
{
    int num_clones = 0;
    char *stack = NULL;
    char *stack_top = NULL;
    
    stack = mmap(NULL, STACK_SIZE, PROT_READ | PROT_WRITE,
                 MAP_PRIVATE | MAP_ANONYMOUS | MAP_STACK, -1, 0);
    if (stack == MAP_FAILED)
    {
        perror("mmap");
        return EXIT_FAILURE;
    }

    stack_top = stack + STACK_SIZE;

    while (clone(idle_func, stack_top, CLONE_PARENT, NULL) != -1)
    {
        num_clones++;
    }
    
    printf("%d\n", num_clones);

    return 0;
}

int main(void)
{
    return do_test();
}
