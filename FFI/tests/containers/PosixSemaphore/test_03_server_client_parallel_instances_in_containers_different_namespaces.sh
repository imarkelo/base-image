#!/bin/bash

if [ "$#" -ne 2 ]; then
    echo "Usage: $0 <semaphore-name> <timeout-seconds>"
    exit 1
fi

# shellcheck source=SCRIPTDIR/setup.sh
. "$(dirname "$(readlink --canonicalize "$0")")"/setup.sh

printf "%s\n" "-- Running named_posix_semaphore in server container in $BAD_CONTAINER."
# shellcheck disable=SC2154
podman exec -d $BAD_CONTAINER bash -c "$(get_path_to_script "$file") $1 $2" > /dev/null

sleep 2

printf "%s\n" "-- Running named_posix_semaphore in client container. Expected: failure."
# shellcheck disable=SC2154
podman exec "$cntr_client" ./tst_named_posix_semaphore "$1" "$2"