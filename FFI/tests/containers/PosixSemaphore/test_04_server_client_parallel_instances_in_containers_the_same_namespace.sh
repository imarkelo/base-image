#!/bin/bash

if [ "$#" -ne 2 ]; then
    echo "Usage: $0 <semaphore-name> <timeout-seconds>"
    exit 1
fi

# shellcheck source=SCRIPTDIR/setup.sh
. "$(dirname "$(readlink --canonicalize "$0")")"/setup.sh

printf "%s\n" "-- Running named_posix_semaphore in server container in $BAD_CONTAINER."
# shellcheck disable=SC2154
podman exec -d $BAD_CONTAINER bash -c "podman run $CONTAINER_PARAMS --replace --name server $BASE_CONTAINER_IMAGE $(get_path_to_script "$file") $1 $2" > /dev/null

sleep 2

printf "%s\n" "-- Running named_posix_semaphore in client container using the same namespace as a server in $BAD_CONTAINER. Expected: success."
podman exec -d $BAD_CONTAINER bash -c "podman run $CONTAINER_PARAMS --replace --name client --ipc container:server $BASE_CONTAINER_IMAGE /var/tst_named_posix_semaphore $(get_path_to_script "$file") $1 $2" > /dev/null