#!/bin/bash
# shellcheck source=SCRIPTDIR/setup.sh
. "$(dirname "$(readlink --canonicalize "$0")")"/setup.sh

# No interference run
podman exec -it "$cntr_orderly" perf stat -e "$DTLB_LOAD" -a sleep 20s > run1.log

# Make interference
podman exec --detach "$cntr_confusion" stress-ng --vm 4 --vm-bytes 150% --vm-keep --timeout 30s > /dev/null

# Interference run
podman exec -it "$cntr_orderly" perf stat -e "$DTLB_LOAD" -a sleep 20s > run2.log

orderly_misses=$(grep "$DTLB_LOAD" run1.log | awk '{print $1}')
interference_misses=$(grep "$DTLB_LOAD" run2.log | awk '{print $1}')
rel_difference=$((interference_misses*10 - orderly_misses*12))

echo "INFO: Misses without pressure: '$orderly_misses'"
echo "INFO: Misses under pressure: '$interference_misses'"

if [ "$rel_difference" -le 0 ]; then
    error "FAIL: No interference!"
    exit 1
else
    success "PASS: Interference is present!"
fi

