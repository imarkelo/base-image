#!/bin/bash
# shellcheck source=SCRIPTDIR/../../common.sh
. "$(dirname "$(readlink --canonicalize "$0")")"/../../common.sh

# Process program options
while test $# -gt 1; do
  case $1 in
    -a|--container-opts)
      ORDERLY_OPTS="$ORDERLY_OPTS $2"
      CONFUSION_OPTS="$CONFUSION_OPTS $2"
      shift 2
      ;;
    -o|--orderly-opts)
      ORDERLY_OPTS="$ORDERLY_OPTS $2"
      shift 2
      ;;
    -c|--confusion-opts)
      CONFUSION_OPTS="$CONFUSION_OPTS $2"
      shift
      ;;
  esac
done

export DTLB_LOAD="dTLB-load-misses"
export cntr_orderly="orderly"
export cntr_confusion="confusion"
test_image=$(build_container_image perf stress-ng)

run_test_container_from_image "$cntr_orderly" "$test_image" "$ORDERLY_OPTS"
run_test_container_from_image "$cntr_confusion" "$test_image" "$CONFUSION_OPTS"

