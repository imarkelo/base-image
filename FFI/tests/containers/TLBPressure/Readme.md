# Freedom From Interference (FFI) - TLBPressure

## Title: Freedom from interference test with TLB pressure

### Description:
TLBPressure test compares TLB load misses with and without TLB pressure from confusion container running stress-ng.

1. `tlb_interference_test` - checks interference created by `stress-ng --vm 4 --vm-bytes 150% --vm-keep --timeout 30s`

### Test input
Test image: quay.io/centos/centos:stream9

### Expected output
```
...
INFO: Misses without pressure: 'XXXXXXX'
INFO: Misses under pressure: 'XXXXXXX'
PASS: Interference is present!
Terminating..
Starting cleanup...
Cleanup complete
bad_container
```
