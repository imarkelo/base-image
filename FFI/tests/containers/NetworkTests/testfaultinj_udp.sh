#!/bin/bash
if test -z "$1"; then
    echo "Failed to run test, invalid fault command!"
    exit 1
fi
acceptableloss=0
modprobe --verbose sch_netem
if test "$( rpm --query --queryformat="%{VERSION}-%{RELEASE}.%{ARCH}\n" kernel-modules-extra)" != "$(uname -r)"; then
    echo "no match found for installing kernel-modules-extra"
    echo "Aborting the Test"
    exit 1
fi
if ! modprobe -q sch_netem; then
  echo "error loading sch_netem module"
  echo "Aborting the Test"
  exit 1
fi
podman run -d --name orderly stream9-stress
podman run -d --cap-add=NET_ADMIN --name confusion stream9-stress
podman run -d --name partner stream9-stress
partner=$(podman inspect partner | grep IPAddress | tail -1 | awk '{print $NF}' |  sed 's/"//g' | sed 's/,//g')
FAULT_CMD="$1"
podman exec -it confusion sh -c "$FAULT_CMD"
podman exec -it confusion sh -c "tc qdisc show"

#Collecting metrics after injecting the fault#

echo -e "\e[1;33m statistics after injecting the fault : \e[0m"
podman exec -it partner "qperf" &
latency=$(podman exec -it confusion sh -c "qperf -v $partner udp_lat")
confusionlatudp=$(echo "$latency" |sed -n '2p' | tail -1 | awk '{print $3}' )
echo "at confusion latency for udp is $confusionlatudp"
latencyorderly=$(podman exec -it orderly sh -c "qperf -v $partner udp_lat")
orderlylatudp=$(echo "$latencyorderly" | sed -n '2p' | tail -1 | awk '{print $3}')
echo "at orderly latency for udp is $orderlylatudp"
confusionlatudp=$(echo "$confusionlatudp*1000" | bc) 
confusionlatusudp=$(echo "$confusionlatudp*0.05" | bc )
confusionretrans=$(podman exec -it confusion sh -c "netstat -s  | grep retransmitted")
retranscon=$(echo "$confusionretrans" | tail -1 |awk '{print $1}')
echo "retransmitted segments at Confusion are  $retranscon"
orderlyretrans=$(podman exec -it orderly sh -c "netstat -s  | grep retransmitted")
retransord=$(echo "$orderlyretrans" | tail -1 |awk '{print $1}')
echo "retransmitted segments at orderly are $retransord"
pkill qperf
podman exec -it partner sh -c " iperf3 -s" &
sleep 3
packetloss=$(podman exec -it confusion sh -c "iperf3 -c $partner -u -t 10")
plossconfusionudp=$(echo "$packetloss" | sed -n '17p' | tail -1 | awk '{print $12}' | sed 's/%//g'| sed 's/(//g'| sed 's/)//g')
sleep 10
packetlossorderly=$(podman exec -it orderly sh -c "iperf3 -c $partner -u -t 10")
plossorderlyudp=$(echo "$packetlossorderly" | sed -n '17p' | tail -1 | awk '{print $12}' | sed 's/%//g'| sed 's/(//g'| sed 's/)//g')
echo "packet loss at confusion for udp is $plossconfusionudp"
echo "packet loss at orderly for udp is $plossorderlyudp"

#Determining pass fail for Retransmissions, Packet loss and Packet delay#

echo -e "\e[1;33m Evaluating... \e[0m"
if awk "BEGIN {exit !($retransord > $retranscon)}"; then
  echo -e "\e[1;31m Interfernce is present - presence of packet retransmission in Orderly \e[0m"
  exit 1
else
        echo -e "\e[1;32m Interference is not present - No packet retransmission in Orderly \e[0m"
fi
if awk "BEGIN {exit !($plossorderlyudp > $acceptableloss)}"; then
  echo -e "\e[1;31m Interfernce is present - presence of packet loss in Orderly for udp \e[0m"
  exit 1
else
        echo -e "\e[1;32m Interference is not present - No loss in Orderly for udp \e[0m"
fi
if awk "BEGIN {exit !($orderlylatudp > $confusionlatusudp)}"; then
  echo -e "\e[1;31m Interfernce is present - presence of packet delay in Orderly for udp \e[0m"
  exit 1
else
        echo -e "\e[1;32m Interference is not present - No packet delay in Orderly for udp\e[0m"
fi
./cleanup.sh                
