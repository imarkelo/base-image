# Network FFI Test Suite

## tc07

### Title:
Validate that traffic shaping with tc can mitigate network interference coming from a container

### Description:
Test plan: `tc07_traffic_shaping_iperf.fmf`

Executable: `test_traffic_shaping.sh`

#### Test logic:
1. Run the following containers from `stream9-stress` image:
    * `orderly` - emulate workload running in normal environment (ASIL-B) to test if interference is present or not.
    * `orderly_partner` - run `iperf3` server for the `orderly` container to run the test against
    * `partner[1-8]` - run servers for multiple `iperf3` processes run from `qm`/`bad_container`.
    * `bad_container` - created if the test is not run with `FFI_QM_SCENARIO` set
2. Measure network bandwidth between `orderly` and `orderly_partner` with `iperf3`
3. Start 8 parallel `iperf3` workloads from `qm`/`bad_container`
4. Measure network bandwidth between `orderly` and `orderly_partner` again
5. If the second measurement is less than 66% of the first measurement - interference detected as expected
6. Apply traffic shaping to the `qm`/`bad_container` NIC with `tc`
7. Measure network bandwidth between `orderly` and `orderly_partner` again
8. The measurement should not be less than 66% of the first one - meaning the interference was mitigated

### Test input
`stream9-stress` container image with `iperf3` installed

### Expected output

```
-- Starting test ./test_traffic_shaping.sh
Creating Containers
...
Starting iperf servers
...
Running iperf
Starting interference
...
Running iperf between orderly and orderly_partner under interference
Running iperf
Normal: 8305 Stress: 957
Bandwidth interference is present as expected
Applying traffic shaping to the qm NIC veth0
Applying traffic shaping on veth0
Running iperf between orderly and orderly_partner under interference with traffic shaping applied
Running iperf
Normal: 8305 Stress: 7411
Bandwidth interference is not present as expected
...
```


## tc(01-06)

Setup for which includes installing podman, pulling the image and then build the image using relevant epel, iputils and sysstat installation.

1. test_icmp.sh -> tc01_network_stress_send_large_packets_icmp part of test.fmf:
Two containers running in a vm confusion and orderly:
Firstly, Orderly pings partner container 10 times in ideal conditions (time noted) and then confusion sends large size packets using  `ping -s` while orderly again pings partner container 10 times (while in network stress due to confusion container)time noted again and compared with time taken in ideal conditions.The metrics such as Packet delay, loss and errors are monitored using ping and sar. Evaluations present are - time delay should not be more than 5% when orderly pings partner container under stress and packet loss should not be more than 0%.

2. test_icmp.sh ->tc02_network_stress_ping_flood_icmp part of test.fmf:
Two containers running in a vm confusion and orderly:
Firstly, Orderly pings partner container 10 times in ideal conditions (time noted) and then confusion creates a ping storm using  `ping -f` while orderly again pings partner conrtainer 10 times (while in network stress or ping flood from confusion container)time noted again and compared with time taken in ideal conditions. The metrics such as Packet delay, loss and errors are monitored using ping and sar. Evaluations present are - time delay should not be more than 5% when orderly pings partner container under stress and packet loss should not be more than 0%.

3. testfaultinj_udp.sh ->tc03_network_packet_loss_injection.fmf:
Three containers running in a vm confusion, orderly and partner:
A packet loss of 5% is injected in confusion container using netem.Then the UDP packet loss is checked in confusion and orderly respectively using `iperf3`.If the packet loss in orderly comes out to be more than 0% then we conclude that interference is present.

4. testfaultinj_udp.sh ->tc04_network_packet_delay_injection.fmf:
Three containers running in a vm confusion, orderly and partner:
A packet delay of 700ms is injected in confusion container using netem.Then the UDP packet delay is checked in confusion and orderly respectively using `qperf`.If the packet delay in orderly comes out to be more than 5% of what in confusion then we conclude that interference is present.For doing this comparison the values are bought to common unit `us`.

5. testfaultinj_tcp.sh ->tc05_network_packet_reordering_tcp.fmf:
Three containers running in a vm confusion, orderly and partner:
A packet reordering fault of `reorder 35% 70%` is injected in confusion container using netem.Then TCP packet loss, packet delay and retransmissions are evaluated.

6. testfaultinj_tcp.sh ->tc06_network_packet_corruption_tcp.fmf:
Three containers running in a vm confusion, orderly and partner:
A packet corruption fault of 10% is injected in confusion container using netem. Then TCP packet loss, packet delay and retransmissions are evaluated.

## The metrics for these tests are collected using :
1. `podman ps --all` for listing containers.
2. `sar` for network statistics.
3. `ping` for timing and packet loss. -ICMP
4. `iperf3` for packet loss. 
5. `qperf` for packet delay. 
6. `mtr` for packet loss in TCP. 