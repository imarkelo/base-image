#!/bin/bash
#set -x

# shellcheck source=SCRIPTDIR/../../common.sh
. "$(dirname "$(readlink --canonicalize "$0")")"/../../common.sh

start_iperf_server_on() {
    local cntr_name=$1
    podman exec -dit "$cntr_name" sh -c "iperf3 -s --logfile iperf_server_out.txt"
}

logfile="/var/tmp/iperf_out.log"

# arg1: container name from which to run iperf3
# arg2: ip address of the iperf3 server
# arg3: (optional) time
# ard4: (optional) if no arg passed - wait, if any arg passed - detach
iperf_to() {
    local cntr_name=$1
    local to_ip=$2
    local time=""
    test -n "$3" && time=$3
    time=${time:-"10"}
    local detach=$4

    podman exec -it "$cntr_name" sh -c "rm -rf $logfile"
    local command="iperf3 -c $to_ip -t $time -f m --logfile $logfile"
    if [ -z "$detach" ]; then
        echo "Running iperf"
        podman exec -it "$cntr_name" sh -c "$command"
    else
        echo "Running iperf in background"
        podman exec -dit "$cntr_name" sh -c "$command"
    fi
}

# arg1: container name from which to extract iperf result
get_iperf_result() {
    local cntr_name=$1
    podman exec -it "$cntr_name" sh -c "grep sender $logfile" | awk '{ print $7 }'
}

# arg1: normal result
# arg2: stress result
# arg3: expected:  1 if intereference expected, 0 if not expected
check_interference() {
    local normal=$1
    local stress=$2
    local expected=$3

    echo "Normal: $normal Stress: $stress"

    local interference=0
    [[ $(bc <<< "$normal / 1.5") -gt $stress ]] && interference=1

    if [[ "$interference" == 1 ]]; then
        msg="Bandwidth interference is present"
    else
        msg="Bandwidth interference is not present"
    fi

    if [[ "$interference" == "$expected" ]]; then
        success "$msg as expected"
    else
        error "$msg though expected otherwise"
        exit 1
    fi
}

# arg1: interface
# arg2: limit
apply_traffic_shaping() {
    local interface=$1
    local limit=$2

    # egress traffic
    echo "Applying traffic shaping on $interface"

    tc qdisc add dev "$interface" root handle 1: htb default 20
    tc class add dev "$interface" parent 1: classid 1:1 htb rate "$limit"kbit prio 5

    ceil=$((95*limit/100))
    rate=$((20*limit/100))
    tc class add dev "$interface" parent 1:1 classid 1:10 htb rate "$rate"kbit ceil "$ceil"kbit prio 1
    rate=$((40*limit/100))
    tc class add dev "$interface" parent 1:1 classid 1:20 htb rate "$rate"kbit ceil "$ceil"kbit prio 2
    rate=$((20*limit/100))
    tc class add dev "$interface" parent 1:1 classid 1:30 htb rate "$rate"kbit ceil "$ceil"kbit prio 3

    tc qdisc add dev "$interface" parent 1:10 handle 10: sfq perturb 10
    tc qdisc add dev "$interface" parent 1:20 handle 20: sfq perturb 10
    tc qdisc add dev "$interface" parent 1:30 handle 30: sfq perturb 10

    tc filter add dev "$interface" parent 1: protocol ip prio 10 u32 match ip tos 0x10 0xff flowid 1:10
    tc filter add dev "$interface" parent 1: protocol ip prio 11 u32 match ip protocol 1 0xff flowid 1:10
    tc filter add dev "$interface" parent 1: protocol ip prio 12 u32 match ip protocol 6 0xff match u8 0x05 0x0f at 0 match u16 0x0000 0xffc0 at 2 flowid 1:10
    tc filter add dev "$interface" parent 1: protocol ip prio 18 u32 match ip dst 0.0.0.0/0 flowid 1:20

    # ingress traffic
    modprobe ifb numifbs=1
    ip link set dev ifb0 up
    tc qdisc add dev "$interface" handle ffff: ingress
    tc filter add dev "$interface" parent ffff: protocol ip u32 match u32 0 0 action mirred egress redirect dev ifb0

    tc qdisc add dev ifb0 root handle 2: htb
    tc class add dev ifb0 parent 2: classid 2:1 htb rate 8192kbit
    tc filter add dev ifb0 protocol ip parent 2: prio 1 u32 match ip src 0.0.0.0/0 flowid 2:1
}

# arg1: interface
remove_traffic_shaping() {
    local interface=$1

    # Remove all qdisc's, ignore return code
    tc qdisc del dev "$interface" root    2> /dev/null > /dev/null || true;
    tc qdisc del dev "$interface" ingress 2> /dev/null > /dev/null || true;
    tc qdisc del dev ifb0         root    2> /dev/null > /dev/null || true;
    tc qdisc del dev ifb0         ingress 2> /dev/null > /dev/null || true;
    modprobe -rv ifb
}

# Override __local_cleanup() defined in common.sh to cleanup correctly
__local_cleanup() {
    remove_traffic_shaping "$bad_container_nic"
}

test_image="stream9-stress"
container_options="--stop-signal KILL"
export BAD_CONTAINER_IMAGE=$test_image
export BAD_CONTAINER_OPTIONS=$container_options
cntr_orderly="orderly"
cntr_orderly_partner="orderly_partner"

bad_streams=8

echo "Creating Containers"
run_test_container_from_image $cntr_orderly $test_image "$container_options"
run_test_container_from_image $cntr_orderly_partner $test_image "$container_options"
for i in $(seq 1 $bad_streams); do
    run_test_container_from_image "partner$i" $test_image "$container_options"
done

orderly_partner_ip=$(get_container_ip $cntr_orderly_partner)
bad_container_nic=$(get_container_nic_name "$BAD_CONTAINER")

echo "Starting iperf servers"
start_iperf_server_on $cntr_orderly_partner
for i in $(seq 1 $bad_streams); do
    start_iperf_server_on "partner$i"
done

# Additional wait until the containers are ready
sleep 1

iperf_to $cntr_orderly "$orderly_partner_ip" "5"
iperf_normal_result=$(get_iperf_result $cntr_orderly)

echo "Starting interference"
for i in $(seq 1 $bad_streams); do
    partner_ip=$(get_container_ip "partner$i")
    iperf_to "$BAD_CONTAINER" "$partner_ip" "120" detach
done

# Wait for the network load to spin up
sleep 3

echo "Running iperf between $cntr_orderly and $cntr_orderly_partner under interference"
iperf_to $cntr_orderly "$orderly_partner_ip" "5"
iperf_stress_result=$(get_iperf_result $cntr_orderly)
check_interference "$iperf_normal_result" "$iperf_stress_result" 1

echo "Applying traffic shaping to the $BAD_CONTAINER NIC $bad_container_nic"
apply_traffic_shaping "$bad_container_nic" "8192"

# Wait for the network shaping to take effect
sleep 3

echo "Running iperf between $cntr_orderly and $cntr_orderly_partner under interference with traffic shaping applied"
iperf_to $cntr_orderly "$orderly_partner_ip" "5"
iperf_stress_result=$(get_iperf_result $cntr_orderly)
check_interference "$iperf_normal_result" "$iperf_stress_result" 0
