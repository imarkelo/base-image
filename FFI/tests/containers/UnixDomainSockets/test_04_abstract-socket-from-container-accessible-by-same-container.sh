#!/bin/bash

# shellcheck source=SCRIPTDIR/setup.sh
. "$(dirname "$(readlink --canonicalize "$0")")"/setup.sh

printf "%s\n" "-- Running af_unix test in orderly as server"
# shellcheck disable=SC2086
podman run $CONTAINER_PARAMS -d --name orderly "$BASE_CONTAINER_IMAGE" ./tst_af-unix > /dev/null

printf "%s\n" "-- Running af_unix test in orderly as client"
podman exec orderly ./tst_af-unix > /dev/null