#!/bin/bash

# shellcheck source=SCRIPTDIR/setup.sh
. "$(dirname "$(readlink --canonicalize "$0")")"/setup.sh

printf "%s\n" "-- Running af_unix test in $BAD_CONTAINER. Failure expected: Timeout!"
# shellcheck disable=SC2154
podman exec $BAD_CONTAINER "$(get_path_to_script "$file")" &

printf "%s\n" "-- Running af_unix."
./tst_sys_af-unix
