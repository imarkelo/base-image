#!/bin/bash

# shellcheck source=SCRIPTDIR/setup.sh
. "$(dirname "$(readlink --canonicalize "$0")")"/setup.sh

printf "%s\n" "-- Running af_unix in assist container. Failure expected: Timeout!"
# shellcheck disable=SC2154
podman exec "$cntr_assist" ./tst_af-unix > /dev/null

printf "%s\n" "-- Running af_unix in $BAD_CONTAINER"
# shellcheck disable=SC2154
podman exec $BAD_CONTAINER "$(get_path_to_script "$file")"
