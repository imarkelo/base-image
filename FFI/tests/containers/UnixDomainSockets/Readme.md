# Freedom From Interference (FFI) - abstract UNIX domain sockets

## Title: Unix Domain Sockets Test Suite

### Description:
This test set should investigate the isolation capabilities across container boundaries.
Included test cases:
 
1. `tc01_abstract-socket-from-system-not-accessible-by-container` - abstract unix domain sockets created in the system are inaccessible in containers
1. `tc02_abstract-socket-from-container-not-accessible-by-system` - abstract unix domain sockets created in one container are inaccessible by the system
1. `tc03_abstract-socket-from-container-not-accessible-by-container` - abstract unix domain sockets created in one container are inaccessible in another container
1. `tc04_abstract-socket-from-container-accessible-by-same-container` - abstract unix domain sockets created in one container are accessible in the same container (to verify test functionality)

### Test input
Test image: quay.io/centos/centos:stream9

### Expected output
1. `tc01_abstract-socket-from-system-not-accessible-by-container (original result: fail)`
```
...
-- Starting test ...
Compiling test for system
Compiling test for containers
...
Compile 'tst_af-unix' in 'builder'
Copying 'tst_af-unix' file
-- Running af_unix. Failure expected: Timeout!
-- Running af_unix test in bad_container. Failure expected: Timeout!
Socket name: sock_test
-- Server mode --
Timeout!
Timeout!
Caught signal! Exiting..
Starting cleanup...
Cleanup complete
builder
assist
bad_container
Terminating..
Nothing to clean up. Terminating...
...
```
2. `tc02_abstract-socket-from-container-not-accessible-by-system (original result: fail)`
```
...
-- Starting test ...
Copying 'tst_af-unix' file
-- Running af_unix test in bad_container. Failure expected: Timeout!
-- Running af_unix.
Socket name: sock_test
-- Server mode --
Socket name: sock_test
-- Server mode --
Timeout!
Caught signal! Exiting..
Starting cleanup...
Cleanup complete
assist
bad_container
Terminating..
Nothing to clean up. Terminating...
...
```
3. `tc03_abstract-socket-from-container-not-accessible-by-container (original result: fail)`
```
...
-- Starting test ...
Copying 'tst_af-unix' file
-- Running af_unix test in orderly. Failure expected: Timeout!
-- Running af_unix test in bad_container.
Socket name: sock_test
-- Server mode --
Timeout!
Caught signal! Exiting..
Starting cleanup...
Cleanup complete
assist
bad_container
Terminating..
Nothing to clean up. Terminating...
...
```
4. `tc04_abstract-socket-from-container-accessible-by-same-container`
```
...
-- Starting test ...
Copying 'tst_af-unix' file
-- Running af_unix test in orderly.
-- Running af_unix test in orderly.
Socket name: sock_test
-- Client mode --
Connection successful!
Terminating..
Starting cleanup...
Cleanup complete
assist
bad_container
```

### Results location:

output.txt.
