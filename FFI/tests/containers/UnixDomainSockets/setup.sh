#!/bin/bash

# shellcheck source=SCRIPTDIR/../../common.sh
. "$(dirname "$(readlink --canonicalize "$0")")"/../../common.sh

# setting up the container
cntr_builder="builder"
cntr_assist="assist"
test_image=$(build_container_image gcc)
file="tst_af-unix"
run_test_container $cntr_assist

if ! test -x tst_sys_af-unix; then
  echo "Preparing test for system"
  gcc -o tst_sys_af-unix -lrt af_unix.c -Wall -Werror
fi

# compile af_unix.c inside the builder container
if ! test -x "$file"; then
  echo "Preparing test for containers"
  run_test_container_from_image "$cntr_builder" "$test_image"
  echo "Compile '$file' in '$cntr_builder'"
  podman exec -it "$cntr_builder" gcc -o "$file" -lrt af_unix.c -Wall -Werror
fi

# copying files
copy_script_to_container $file
echo "Copying '$file' file"
