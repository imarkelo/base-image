#!/bin/bash

# shellcheck source=SCRIPTDIR/../../common.sh
. "$(dirname "$(readlink --canonicalize "$0")")"/../../common.sh

# setting up the container
cntr_builder="builder"
cntr_client="client"
test_image=$(build_container_image gcc)
file="tst_system_v_semaphore"
run_test_container $cntr_client

if ! test -x tst_sys_system_v_semaphore; then
  echo "Preparing test for system"
  gcc -o tst_sys_system_v_semaphore -lrt system_v_semaphore.c -Wall -Werror
fi

# compile system_v_semaphore.c inside the builder container
if ! test -x "$file"; then
  echo "Preparing test for containers"
  run_test_container_from_image "$cntr_builder" "$test_image"
  echo "Compile '$file' in '$cntr_builder'"
  podman exec -it "$cntr_builder" gcc -o "$file" -lrt system_v_semaphore.c -Wall -Werror
fi

# copying files
copy_script_to_container $file
echo "Copying '$file' file"
