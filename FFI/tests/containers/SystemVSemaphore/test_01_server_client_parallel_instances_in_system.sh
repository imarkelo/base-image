#!/bin/bash

# shellcheck source=SCRIPTDIR/setup.sh
. "$(dirname "$(readlink --canonicalize "$0")")"/setup.sh

printf "%s\n" "-- Running system_v_semaphore as server."
./tst_sys_system_v_semaphore &

printf "%s\n" "-- Running system_v_semaphore as client."
./tst_sys_system_v_semaphore
