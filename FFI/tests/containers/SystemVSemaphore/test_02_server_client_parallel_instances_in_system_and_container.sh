#!/bin/bash

# shellcheck source=SCRIPTDIR/setup.sh
. "$(dirname "$(readlink --canonicalize "$0")")"/setup.sh

printf "%s\n" "-- Running system_v_semaphore as a server (detached mode with $BAD_CONTAINER)."
# shellcheck disable=SC2154
podman exec -d $BAD_CONTAINER "$(get_path_to_script "$file")" > /dev/null

printf "%s\n" "-- Running system_v_semaphore as a client in system. Expected: failure, not able to access the same semaphore."
./tst_sys_system_v_semaphore 