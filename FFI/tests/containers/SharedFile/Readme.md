# Freedom From Interference (FFI) - Shared socket/fd by mounting as a volume

This test set investigate a socket/file descriptor shared between two containers by mounting it as a volume.

## Test cases

All the test cases need these input parameters:

    - '--test-name': Specifies the name of the test case (e.g., "SharedFile").
    - '--file-name': Specifies the file name used in the test (e.g., "hello", with its content set to "Hello FFI test!").
    - '--mount': (Optional) If provided, enables mounting a file volume on the host. No argument is required.
    - '--timeout-expected': (Optional) If provided, treats a timeout as an expected result. No argument is required.

    Note: '--mount' and '--timeout-expected' are flags. If they are present, they enable the corresponding behavior.

1. File descriptor shared via socket between server and client on host, without mount as a volume

   Expected result:

        Compile 'tst_socket_fd' in 'builder'
        Copying 'tst_socket_fd' file
        Copying 'hello' file
        -- Running server on host without mount file volume
        -- Running client on host without mount file volume
        - - - Server start - - -
        - - - Test read access - - -
        FAIL:SharedFile: Read unmounted volume is FORBIDDEN!!!
        Terminating..
2. File descriptor shared via socket between server and client running on host, mounting as a volume

   Expected result:

        Copying 'tst_socket_fd' file
        Copying 'hello' file
        -- Running server on host with mount file volume
        -- Running client on host with mount file volume
        - - - Client start - - -
        - - - Send file - - -
        Terminating..
3. File descriptor shared via socket between host and a qm container without mount as a volume

   Expected result:

        -- Running server container in qm
        -- Running client on host, Expected: Timeout!
4. File descriptor shared via socket between host and a qm container mounting as a volume

   Expected result:

        -- Running server container in qm
        -- Running client on host, Expected: Timeout!
5. File descriptor shared via socket between two containers in qm without mount as a volume

   Expected result:

        -- Running server container in qm
        -- Running client container in qm
        Terminating..
6. File descriptor shared via socket between two containers in qm mounting as a volume

   Expected result:

        -- Running server container in qm
        -- Running client container in qm
        Terminating..

Results location:

    output.txt.
