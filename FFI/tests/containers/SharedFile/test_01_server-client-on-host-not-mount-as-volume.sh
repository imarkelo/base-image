#!/bin/bash

# shellcheck source=SCRIPTDIR/setup.sh
. "$(dirname "$(readlink --canonicalize "$0")")"/setup.sh

printf "%s\n" "-- Running server without mount file volume on host"
./tst_asilb_socket_fd --test-name $tst_name --file-name $f_name &

printf "%s\n" "-- Running client without mount file volume on host"
./tst_asilb_socket_fd --test-name $tst_name --file-name $f_name > /dev/null
