// SPDX-License-Identifier: GPL-2.0-or-later
/*
 * Copyright Red Hat
 * Author: Dennis Brendel <dbrendel@redhat.com>
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>

#include <sys/mman.h>

#define KBYTE 1024L
#define MBYTE KBYTE*1024L

/* simple test that initializes a certain amount of memory while measuring the
 * time it took
 * */

int main(int argc, char* argv[]) {

  srand(time(NULL));
  const long page_size = sysconf(_SC_PAGESIZE);
  void* memptr = NULL;
  struct timespec ts_start, ts_end;
  long memsize = argc > 1 ? atol(argv[1]) : 3700;
  

  fprintf(stderr, "Test is starting!\n");

  clock_gettime(CLOCK_MONOTONIC, &ts_start);
  memptr = mmap(NULL, memsize * MBYTE, PROT_READ | PROT_WRITE,
                MAP_ANONYMOUS | MAP_PRIVATE, -1, 0);

  for (long i = 0; i < memsize * MBYTE - page_size; i+=page_size) {
    memset(memptr+i, rand(), sizeof(int));
  }
  clock_gettime(CLOCK_MONOTONIC, &ts_end);
  long long duration_ns = ts_end.tv_nsec - ts_start.tv_nsec
                        + (ts_end.tv_sec - ts_start.tv_sec)
                        * 1000 * 1000 * 1000;
  fprintf(stderr, "memset() %ld MB in %.0f ms\n",
                  memsize, (double)duration_ns / (1000 * 1000));


  return EXIT_SUCCESS;
}
