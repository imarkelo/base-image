#!/bin/bash
# shellcheck source=SCRIPTDIR/setup_iotests.sh
. "$(dirname "$(readlink --canonicalize "$0")")"/setup_iotests.sh

# Run orderly without interference 
echo "Starting first run"
# shellcheck disable=SC2086
podman exec -it orderly ioping $IOPING_OPTS /test > run1.log

# Run confusion to start interference
if test -n "$FFI_QM_SCENARIO"; then
    echo "Starting interference using qm: $STRESS_CMD_QM"
    # shellcheck disable=SC2086
    podman exec -itd qm $STRESS_CMD_QM > /dev/null
else
    echo "Starting interference using: $STRESS_CMD"
    # shellcheck disable=SC2086
    podman run --detach $CONFUSION_OPTS -v /var/test_confusion:/test:Z --workdir /test --name confusion "$test_image" $STRESS_CMD > /dev/null
fi
# Give stress-ng a moment to start
sleep 10

# Run orderly with interference
echo "Starting second run"
# shellcheck disable=SC2086
podman exec -it orderly ioping $IOPING_OPTS /test > run2.log

# Cleanup containers and directories
podman rm --force orderly > /dev/null
podman rm --force confusion > /dev/null
rm -rf /var/test_orderly
rm -rf /var/test_confusion

# Process ioping output 
xfer1=$(cut -d ' ' -f4 < run1.log)
xfer2=$(cut -d ' ' -f4 < run2.log)

# Interference presence check
max_rel_error=$(( xfer1 / 5 ))
xfer_difference=$(( xfer1 - xfer2 ))
xfer_difference=${xfer_difference#-}  # abs()
echo "First  run I/O throughput is $((xfer1 / 1000))kb/s"
echo "Second run I/O throughput is $((xfer2 / 1000))kb/s"
if [ "$xfer_difference" -gt "$max_rel_error" ]; then
    error "Interference is present!"
    exit 1
else
    success "No interference!"
fi

