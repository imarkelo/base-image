#!/bin/bash

# Name: environment_info.sh
# Maintainer: Julia Graham <jugraham@redhat.com>
#
# Version: 1.1.1
#
# Usage:
#   To run the script, execute the following commands:
#   export TF_ENDPOINT=<The Testing Farm Endpoint>
#   ./environment_info.sh

function run_cmd() {
	# $1: Command to be executed

	cmd="$1"
	echo -e "\n_______________\ncmd> $cmd\n" >>"$logfile"
	eval "$cmd" &>>"$logfile"
}

function lscpu_info() {
	# $1: Row of information to find

	echo -e "  $(echo "$lscpuResult" | grep "^${1}:" | tr -s ' ')" | tee -a "$logfile"
}

export PATH=$PATH:/usr/local/sbin:/usr/sbin

# Prepare environment
logfile=environment_information.log

# Start scan
echo -e "Environment Information:\n========================\n" >"$logfile"

# Summary
echo
echo -e "Summary:\n" | tee -a "$logfile"

if [ -f /etc/os-release ]; then
	# shellcheck disable=SC1091
	source /etc/os-release
	echo -e "OS: ${NAME} release ${VERSION_ID}" | tee -a "$logfile"
fi

lscpuResult=$(lscpu)
echo "CPU:" | tee -a "$logfile"
lscpu_info "Vendor ID"
lscpu_info "Model name"
lscpu_info "Model"
lscpu_info "Stepping"
lscpu_info "Thread(s) per core"
lscpu_info "Core(s) per socket"
lscpu_info "CPU(s)"

if [ -f /etc/build-info ]; then
	# shellcheck disable=SC1091
	source /etc/build-info
	echo -e "Build Info:" | tee -a "$logfile"
	echo -e "  Release: ${RELEASE}\n  UUID: ${UUID}\n  Timestamp: ${TIMESTAMP}\n  Image name: ${IMAGE_NAME}\n  Image target: ${IMAGE_TARGET}" | tee -a "$logfile"
fi

echo -e "Release Details:" | tee -a "$logfile"

echo -e "  tmt version: ${TMT_VERSION:-n/a}" | tee -a "$logfile"
[ -n "${TF_ENDPOINT}" ] && TF_VERSION=$(curl -s "$(dirname "$TF_ENDPOINT")/about" | grep -o '"version": *"[^"]*' | grep -oE '[^"]+$') || TF_VERSION=n/a
echo -e "  Testing Farm version: $TF_VERSION" | tee -a "$logfile"

# Additional Information
echo -e "\n\nAdditional Information:" >>"$logfile"

run_cmd 'cat /etc/os-release'
run_cmd 'lscpu'
run_cmd 'lsmem'
run_cmd 'lsblk'
run_cmd 'lsmod'
run_cmd 'cat /etc/build-info'

if [ -d "${TMT_PLAN_DATA}" ]; then
	cp $logfile "${TMT_PLAN_DATA}"/$logfile
fi

# Wrap up
echo
echo "Environment information can be found in '$logfile'."
exit 0
